<?php

namespace App\Controller;

use App\Entity\Request as EntityRequest;
use App\Entity\Reviews;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;




class MaleteoController extends AbstractController
{



     /**
      * @Route ("/maleteo", methods={"GET","POST"}, name="postForm")
      */

     public function postInquiry(Request $request, EntityManagerInterface $em)
     {

          $name = $request->get('nombre');
          $email = $request->get('mail');
          $ciudad = $request->get('ciudad');


          if ($name && $email && $ciudad != false) {
               $petition = new EntityRequest();
               $petition->setName($name);
               $petition->setEmail($email);
               $petition->setCiudad($ciudad);

               //BBDD
               $em->persist($petition);
               $em->flush();

               return $this->render('maleteo/form_sent_maleteo.html.twig');
          } else {
               return $this->render('maleteo/form_maleteo.html.twig');
          }
     }

     /**
      * @Route ("/maleteo/opiniones", methods={"GET", "POST"}, name="reviews")
      */
     public function Reviews(Request $request, EntityManagerInterface $em)
     {
          $name = $request->get('nombre');
          $text = $request->get('text');
          $city = $request->get('ciudad');

          if ($name && $text && $city != false) {
               $review = new Reviews();
               $review->setNombre($name);
               $review->setOpinion($text);
               $review->setCiudad($city);

               $em->persist($review);
               $em->flush();

               return $this->render('maleteo/opinion_enviada.html.twig');
          } else {
               return $this->render('maleteo/nueva_opinion.html.twig');
          }
     }

     /**
      * @Route ("/maleteo/admin", name="admin_peticiones")
      */

     public function VerPeticiones(EntityManagerInterface $em)
     {
          $repo = $em->getRepository(EntityRequest::class);
          $peticion = $repo->findAll();

          return $this->render(
               'maleteo/ver_peticion.html.twig',
               [
                    "peticion" => $peticion
               ]
          );
     }

     /**
      * @Route ("/maleteo/admin/reviews", name="admin_review")
      */

     public function VerReviews(EntityManagerInterface $em)
     {
          $repo = $em->getRepository(Reviews::class);
          $review = $repo->findAll();

          return $this->render(
               'maleteo/ver_opinion.html.twig',
               [
                    "opinion" => $review
               ]
          );
     }
}
